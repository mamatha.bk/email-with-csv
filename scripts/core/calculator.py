import pandas as pd
import csv

df = pd.read_csv('E:\\task3.csv')
cols = pd.read_csv
max_time_stamp = []
min_time_stamp = []
max_col = df.max()
min_col = df.min()
av_col = df.mean()
time_stamp1 = []
time_stamp2 = []

with open('E:\\task3.csv', 'r') as file:
    reader = csv.reader(file)
    data = list(reader)

    max_col1 = list(max_col)
    min_col1 = list(min_col)
    av_col1 = list(av_col)
    for i in range(1, 6):
        max_time = []
        min_time = []
        av_time = []
        for j in range(1, 97):
            if float(data[j][i]) == float(max_col1[i]):
                max_time.append(data[0][i])
                max_time.append(float(max_col1[i]))
                max_time.append(data[j][0])
                time_stamp1.append(data[j][0])
                max_time_stamp.append(max_time)
            if float(data[j][i]) == float(min_col1[i]):
                min_time.append((data[0][i]))
                min_time.append(float(min_col1[i]))
                min_time.append(data[j][0])
                time_stamp2.append(data[j][0])
                min_time_stamp.append(min_time)
    print(time_stamp1)
    with open('E:\\result.csv', mode='w') as csv_file:
        fieldnames = []
        fieldnames.append("DATA")
        for i in range(1, 6):
            fieldnames.append(data[0][i])
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow({"DATA": "minimum", "kWh": min_col[1], "kVAh": min_col[2], "kW": min_col[3], "kVA": min_col[4],
                         "current": min_col[5]})
        writer.writerow(
            {"DATA": "timestamps", "kWh": time_stamp2[0], "kVAh": time_stamp2[1], "kW": "null", "kVA": time_stamp2[2],
             "current": time_stamp2[3]})
        writer.writerow({"DATA": "maximum", "kWh": max_col[1], "kVAh": max_col[2], "kW": max_col[3], "kVA": max_col[4],
                         "current": max_col[5]})
        writer.writerow({"DATA": "timestamps", "kWh": time_stamp1[0], "kVAh": time_stamp1[1], "kW": time_stamp1[2],
                         "kVA": time_stamp1[3], "current": time_stamp1[4]})
        writer.writerow({"DATA": "Average", "kWh": av_col[0], "kVAh": av_col[1], "kW": av_col[2], "kVA": av_col[3],
                         "current": av_col[4]})
